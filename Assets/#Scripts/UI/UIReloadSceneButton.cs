﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIReloadSceneButton : MonoBehaviour
{
    [SerializeField] protected Button _reloadButton = null;
    
    
    protected virtual void Start()
    {
        _reloadButton.onClick.AddListener(OnReloadButtonClick);
    }

    protected virtual void OnReloadButtonClick()
    {
        ReloadCurrentScene();
    }

    protected virtual void ReloadCurrentScene()
    {
        var activeScene = SceneManager.GetActiveScene();
        int sceneId = activeScene.buildIndex;
        SceneManager.LoadScene(sceneId);
    }
}
