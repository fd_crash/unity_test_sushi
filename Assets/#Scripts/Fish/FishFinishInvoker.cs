﻿using System;
using UnityEngine;

public class FishFinishInvoker : MonoBehaviour
{
    [Header("TARGET")]
    [SerializeField] protected Transform _targetItem = null;
    [SerializeField] protected Transform _targetFinishPoint = null;
    [SerializeField] protected float _targetFinishMovementSpeed = 1f;


    protected KnifeMoveController _knifeMoveController = null;

    protected Vector3 _defaultPosition = Vector3.zero;
    

    protected virtual void Awake()
    {
        Initialize();
        
        _defaultPosition = _targetItem.position;
        
        ResetState();
    }

    protected virtual void OnDestroy()
    {
        if (_knifeMoveController != null)
        {
            _knifeMoveController.onFinish -= OnFinishState;
        }
    }

    protected virtual void Initialize()
    {
        var knifeMoveController = FindObjectOfType<KnifeMoveController>();
        Initialize(knifeMoveController);
    }

    protected virtual void Initialize(KnifeMoveController knifeMoveController)
    {
        _knifeMoveController = knifeMoveController;
        if (_knifeMoveController == null)
        {
            return;
        }

        knifeMoveController.onFinish += OnFinishState;
    }

    protected virtual void Update()
    {
        var currentPosition = _targetItem.position;
        var targetPosition = _targetFinishPoint.position;

        float deltaTime = Time.deltaTime;
        float currentSpeed = _targetFinishMovementSpeed * deltaTime;

        var newPosition = Vector3.MoveTowards(currentPosition, targetPosition, currentSpeed);
        _targetItem.position = newPosition;

        float deltaDistance = Vector3.Distance(newPosition, targetPosition);

        if (Mathf.Approximately(deltaDistance, 0f))
        {
            enabled = false;
        }
    }

    public virtual void ResetState()
    {
        enabled = false;
        
        ResetPositionToDefault();
    }

    public virtual void PlayFinish()
    {
        enabled = true;
    }

    protected virtual void ResetPositionToDefault()
    {
        _targetItem.position = _defaultPosition;
    }

    protected virtual void OnFinishState(KnifeMoveController knifeMoveController)
    {
        PlayFinish();
    }
}
