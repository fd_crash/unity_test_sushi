﻿using System;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.UI;

public class FishRenderTextureController : MonoBehaviour
{
    [SerializeField] protected Camera _targetCamera = null;
    [SerializeField] protected Vector2Int _targetTextureSize = new Vector2Int(128, 256);
    [SerializeField] protected Renderer[] _targetRenderers = new Renderer[0];

    [SerializeField] protected RawImage _previewTextureRawImage = null;

    
    protected RenderTexture _createdRenderTexture = null;
    protected Material[] _createdMaterials = null;
    
    
    protected virtual void Awake()
    {
        CheckTargets();
        
        ResetState();
    }

    protected virtual void OnDestroy()
    {
        if (_createdRenderTexture != null)
        {
            RenderTexture.ReleaseTemporary(_createdRenderTexture);
        }
        
        if (_createdMaterials != null)
        {
            for (int i = 0; i < _createdMaterials.Length; i++)
            {
                var createdMaterial = _createdMaterials[i];
                GameObject.Destroy(createdMaterial);
            }
        }
    }

    protected virtual void OnEnable()
    {
        ResetState();
    }

    public virtual void ResetState()
    {
        ResetRenderTexture();
    }

    protected virtual void CheckTargets()
    {
        if (_targetCamera == null)
        {
            return;
        }
        
        _targetCamera.clearFlags = CameraClearFlags.Nothing;

        _createdRenderTexture =
            RenderTexture.GetTemporary(_targetTextureSize.x, _targetTextureSize.y);
        _targetCamera.targetTexture = _createdRenderTexture;
        _previewTextureRawImage.texture = _createdRenderTexture;

        _createdMaterials = new Material[_targetRenderers.Length];
        
        for (int i = 0; i < _targetRenderers.Length; i++)
        {
            var targetRenderer = _targetRenderers[i];
            var targetMaterial = targetRenderer.material;
            targetMaterial.SetTexture(ShaderProperties.RenderTex, _createdRenderTexture);
            targetRenderer.material = targetMaterial;
        
            _createdMaterials[i] = targetMaterial;
        }
    }

    protected virtual void ResetRenderTexture()
    {
        if (_targetCamera == null)
        {
            return;
        }
        
        _targetCamera.clearFlags = CameraClearFlags.SolidColor;
        
        _targetCamera.enabled = false;
        _targetCamera.Render();
        _targetCamera.enabled = true;
        
        _targetCamera.clearFlags = CameraClearFlags.Nothing;
    }
    
    protected static class ShaderProperties
    {
        public static readonly int RenderTex = Shader.PropertyToID("_RenderTex");
    }
}
