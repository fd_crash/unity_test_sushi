﻿using System;
using UnityEngine;

[ExecuteAlways]
public class KnifeBladeHeightMaskController : MonoBehaviour
{
    [Header("HEIGHTS")]
    [SerializeField] protected float _minHeight = 0f;
    [SerializeField] protected float _maxHeight = 0f;
    
    [Header("COLORS")]
    [SerializeField] protected Color _lowColor = Color.black;
    [SerializeField] protected Color _heightColor = Color.white;
    [SerializeField] protected AnimationCurve _heightAnimationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
    
    [Header("TARGET")]
    [SerializeField] protected Transform _targetTransform = null;
    [SerializeField] protected Renderer _targetRenderer = null;


    protected Material _targetMaterial = null;


    protected virtual void Awake()
    {
        CheckTargets();
    }

    protected virtual void Start()
    {
        CheckCurrentColor();
    }

    protected virtual void OnEnable()
    {
        CheckTargets();
    }

    protected virtual void Update()
    {
        CheckCurrentColor();
    }

    protected virtual void CheckTargets()
    {
        if (_targetRenderer == null || _targetTransform == null)
        {
            enabled = false;

            return;
        }

        _targetMaterial = _targetRenderer.sharedMaterial;
    }

    protected virtual void CheckCurrentColor()
    {
        if (_targetMaterial == null)
        {
            return;
        }

        float currentHeight = _targetTransform.position.y;
        CheckCurrentColor(currentHeight);
    }

    protected virtual void CheckCurrentColor(float heightValue)
    {
        var currentColor = GetColorByHeight(heightValue);

        SetCurrentColor(heightValue, currentColor);
    }

    protected virtual void SetCurrentColor(float heightValue, Color color)
    {
        _targetMaterial.color = color;
    }

    protected virtual Color GetColorByHeight(float heightValue)
    {
        float colorT = (heightValue + _minHeight) / _maxHeight;

        return Color.Lerp(_lowColor, _heightColor, _heightAnimationCurve.Evaluate(colorT));
    }
}
