﻿using System;
using UnityEngine;

public class KnifeMoveController : MonoBehaviour
{
    public event Action<KnifeMoveController> onFinish = null;
    
    
    [Header("TARGET")]
    [SerializeField] protected Transform _targetTransform = null;
    
    [Header("CONFIGS")]
    [SerializeField] protected Transform _startPoint = null;
    [SerializeField] protected Transform _finishPoint = null;
    
    [Header("HORIZONTAL")]
    [SerializeField] protected float _horizontalMovementSpeed = 1f;
    
    [Header("VERTICAL")]
    [SerializeField] protected float _verticalMovementSpeed = 1f;
    [SerializeField] protected float _verticalMovementMinLimit = 0f;
    [SerializeField] protected float _verticalMovementMaxLimit = 0.02f;


    protected float _currentHorizontalValue = 0f;
    protected float _currentVerticalValue = 0f;
    
    protected bool _currentTouchState = false;
    protected Vector2 _startTouchPosition = Vector2.zero;
    

    protected virtual void Awake()
    {
        ResetState();
    }

    protected virtual void OnEnable()
    {
        ResetState();
    }

    protected virtual void Update()
    {
        bool hasTouch = HasTouch();

        if (_currentTouchState != hasTouch)
        {
            _startTouchPosition = GetTouchPosition();
            _currentTouchState = hasTouch;
        }
        
        if (!hasTouch)
        {
            return;
        }
        
        float deltaTime = Time.deltaTime;

        float deltaHorizontalValue = GetDeltaHorizontalMovementValue(deltaTime);

        if (Mathf.Approximately(deltaHorizontalValue, 0f))
        {
            return;
        }

        float deltaVerticalValue = GetDeltaVerticalMovementValue(deltaTime);
        
        Move(deltaHorizontalValue, deltaVerticalValue, deltaTime);

        CheckFinishState();
    }

    public virtual void ResetState()
    {
        enabled = true;
        
        ResetPositionToDefault();
    }

    protected virtual void Move(float deltaHorizontalValue, float deltaVerticalValue, float deltaTime)
    {
        float horizontalValue = _currentHorizontalValue + deltaHorizontalValue;
        float verticalValue = _currentVerticalValue + deltaVerticalValue;

        SetPosition(horizontalValue, verticalValue);
    }

    protected virtual void SetPosition(float horizontalValue, float verticalValue)
    {
        var currentPosition = _targetTransform.position;

        float x = currentPosition.x;
        float y = verticalValue;
        float z = horizontalValue;

        y = Mathf.Clamp(y, _verticalMovementMinLimit, _verticalMovementMaxLimit);

        currentPosition.Set(x, y, z);
        _targetTransform.position = currentPosition;
        
        _currentHorizontalValue = horizontalValue;
        _currentVerticalValue = verticalValue;
    }
    
    protected virtual float GetDeltaHorizontalMovementValue(float deltaTime)
    {
        return _horizontalMovementSpeed * deltaTime;
    }

    protected virtual float GetDeltaVerticalMovementValue(float deltaTime)
    {
        var touchPosition = GetTouchPosition();
        var deltaPosition = touchPosition - _startTouchPosition;
        // _startTouchPosition = touchPosition;

        var resolution = GetScreenResolution();

        deltaPosition /= resolution;

        return deltaPosition.y * _verticalMovementSpeed * deltaTime;
    }

    protected virtual void ResetPositionToDefault()
    {
        var pointPosition = _startPoint.position;
        SetPosition(pointPosition.z, pointPosition.y);
    }

    protected virtual void CheckFinishState()
    {
        float horizontalPosition = _currentHorizontalValue;

        var finishPosition = _finishPoint.position;
        float finishHorizontalPosition = finishPosition.z;

        if (horizontalPosition <= finishHorizontalPosition)
        {
            SetFinishState();
        }
    }

    protected virtual void SetFinishState()
    {
        enabled = false;
        
        onFinish?.Invoke(this);
    }

#region TOUCHES

    protected virtual bool HasTouch()
    {
        bool hasTouch = false;
        
        // if (Input.touchSupported)
        // {
        //     hasTouch = Input.touchCount != 0;
        // }
        // else
        {
            hasTouch = Input.GetMouseButton(0);
        }

        return hasTouch;
    }

    protected virtual Vector2 GetTouchPosition()
    {
        Vector2 touchPosition = Vector2.zero;
        
        // if (Input.touchSupported)
        // {
        //     var touch = Input.GetTouch(0);
        //     touchPosition = touch.position;
        // }
        // else
        {
            touchPosition = Input.mousePosition;
        }

        return touchPosition;
    }
    
#endregion TOUCHES


#region SCREEN

    protected virtual Vector2Int GetScreenResolution()
    {
        var currentResolution = Screen.currentResolution;
        var resolution = new Vector2Int(currentResolution.width, currentResolution.height);
        return resolution;
    }

#endregion SCREEN
}
